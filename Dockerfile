FROM debian:bullseye-slim

RUN apt-get update && \
    apt-get install -y bc bison build-essential device-tree-compiler flex \
	gcc-aarch64-linux-gnu gcc-arm-linux-gnueabi git libssl-dev wget

